using System;
using oli.num;

class Numerals{
	static void Main(string[] args)
	{
		RomanNumeralGenerator g = new RomanNumeralGenerator();
		foreach(string arg in args){
			Console.WriteLine(arg + " : " + g.generate(Convert.ToUInt32(arg)));	
		}
	}
}