namespace oli.num {
	public struct NumeralStruct {
		public string numeral;
		public uint number;
		public NumeralStruct(string nl, uint nr)
		{
			number = nr;
			numeral = nl;
		}
	}
}