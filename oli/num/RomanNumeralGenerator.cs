using System;

namespace oli.num {
	public class RomanNumeralGenerator : IRomanNumeralGenerator {
		private NumeralStruct[] numeralStructs = {
			new NumeralStruct("M",	1000),
			new NumeralStruct("CM",	900),
			new NumeralStruct("D", 	500),
			new NumeralStruct("CD",	400),
			new NumeralStruct("C",	100),
			new NumeralStruct("XC",	90),
			new NumeralStruct("L",	50),
			new NumeralStruct("XL",	40),
			new NumeralStruct("X",	10),
			new NumeralStruct("IX",	9),
			new NumeralStruct("V",	5),
			new NumeralStruct("IV",	4),
			new NumeralStruct("I",	1),
			new NumeralStruct("", 	0)
		};
		public RomanNumeralGenerator(){}
		public string generate(uint number) {
			return getNumeral(number);
		}
		private string getNumeral(uint number, string numeral = ""){
			if(number == 0){ return numeral; }
			NumeralStruct s = getNumeralStruct(number);
			return getNumeral(number-s.number, numeral+s.numeral);
		}
		private NumeralStruct getNumeralStruct(uint number){
			foreach(NumeralStruct s in numeralStructs) {
				if(number >= s.number){
					return s;
				}
			}
			return numeralStructs[numeralStructs.Length-1];
		}
	}
}