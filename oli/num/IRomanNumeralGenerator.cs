namespace oli.num {
	public interface IRomanNumeralGenerator {
		string generate(uint number);	
	}
}